import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, TouchableOpacity, NativeEventEmitter, DeviceEventEmitter, NativeModules, NativeAppEventEmitter } from 'react-native';
// var { RNMyLibrary } = NativeModules;
// const {CalendarManager} = NativeModules;

// import CalendarManager from './ToastExample';
// Both DeviceEventEmitterand NativeAppEventEmitter are deprecated, you should use NativeEventEmitter instead.
// const calendarManagerEmitter = new NativeEventEmitter(CalendarManager);
// when it was set as rnmylibrary the function worked but the listener didnt
export default class App extends Component {
  constructor(props) {
    super(props);
    this.didPressToObjcButton = this.didPressToObjcButton.bind(this);
  }
  componentWillMount() {
    const {CalendarManager} = NativeModules;
    this.eventEmitter = new NativeEventEmitter(NativeModules.CalendarManager);

    this.eventEmitter.addListener('CancelEvent', (data) => {
      console.log(data);
    })
      // this.subscription = DeviceEventEmitter.addListener('CancelEvent', (data) => {
      //   console.log(data);
      // })

      // this.subscription = DeviceEventEmitter.addListener('CancelEvent', (reminder) => {
      //   console.log(reminder.name)
      // });

      // this.subscription = DeviceEventEmitter.addListener('OKEvent',(data) => {
      //   console.log(data);
      // })
    // }
  // componentDidMount() {
  //   // this.subscription = DeviceEventEmitter.addListener('Birthday Party', (e) =>  {
  //   //   console.log('EVENT!', e);
  //   // })
  //   this.subscription = DeviceEventEmitter.addListener("video-progress", (data) => {
  //     console.log('DATA', data)
  //   })
  //   console.log('subscription', this.subscription);
  // }
  //
}
didPressToObjcButton() {
    // We'll sent event press button to ObjetiveC
    NativeModules.CalendarManager.showAlert('This is react-native');
  }
  componentWillUnmount() {
    this.subscription.remove();
    // this.eventEmitter.remove();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>Press Cmd+R to reload Cmd+D or shake for dev menu</Text>
        {
          // <TouchableOpacity onPress={() => {
          //   // CalendarManager.alertView('Birthday Party', '4 Privet Drive, Surrey');
          //   CalendarManager.showAlert('This is react-native');
          //   // ToastExample.show('TESTING', ToastExample.SHORT)
          // }}>
        }
        <TouchableOpacity onPress={this.didPressToObjcButton}>
          <Text>TOAST</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
