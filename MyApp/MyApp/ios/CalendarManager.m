#import "CalendarManager.h"
#import <React/RCTLog.h>
#import <React/RCTBridgeModule.h>

@implementation CalendarManager
{
  bool hasListeners;
}

RCT_EXPORT_MODULE();

- (NSArray<NSString*> *)supportedEvents {
  return @[@"CancelEvent", @"OKEvent"];
}

-(void)startObserving {
    hasListeners = YES;
}

-(void)stopObserving {
    hasListeners = NO;
}

#pragma mark - Handler event from React

RCT_EXPORT_METHOD(showAlert:(NSString *)msg) {

  // We'll show UIAlerView to know listener successful.
  UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
  dispatch_async(dispatch_get_main_queue(), ^{
    [alert show];
  });


}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (hasListeners) { // Only send events if anyone is listening
  //   [self sendEventWithName:@"CancelEvent" body:@"Tap on Cancel button from Objc"];;
  // }
  if (buttonIndex == 0) {
    // Sent event tap on Cancel
    [self sendEventWithName:@"CancelEvent" body:@"Tap on Cancel button from Objc"];

  } else if (buttonIndex == 1) {
    // Sent event tap on Ok
    [self sendEventWithName:@"OKEvent" body:@"Tap on OK button from Objc"];
  }
}
}

@end






// {
//   bool hasListeners;
// }
// RCT_EXPORT_MODULE();
// - (NSArray<NSString *> *)supportedEvents
// {
//   return @[@"EventReminder"];
// }
// // Will be called when this module's first listener is added.
// -(void)startObserving {
//     hasListeners = YES;
//     // Set up any upstream listeners or background tasks as necessary
// }
//
// // Will be called when this module's last listener is removed, or on dealloc.
// -(void)stopObserving {
//     hasListeners = NO;
//     // Remove upstream listeners, stop unnecessary background tasks
// }
//
// RCT_EXPORT_METHOD(calendarEventReminderReceived:(NSNotification *)notification)
// {
//   NSString *eventName = notification.userInfo[@"name"];
//   if (hasListeners) { // Only send events if anyone is listening
//     [self sendEventWithName:@"EventReminder" body:@{@"name": eventName}];
//   }
// }
//
// @end

// #import <React/RCTLog.h>
// #import <React/RCTBridgeModule.h>
// #import <React/RCTViewManager.h>
// #import <UIKit/UIKit.h>
// #import <React/RCTViewManager.h>
// #import <React/RCTEventEmitter.h>

// @interface CalendarManager: RCTEventEmitter <RCTBridgeModule,UIAlertViewDelegate>
// @end

// @implementation CalendarManager

// {
//   bool hasListeners;
// }

// RCT_EXPORT_MODULE();

// - (NSArray<NSString*> *)supportedEvents {
//   return @[@"CancelEvent", @"OKEvent"];
// }
//
// #pragma mark - Handler event from React
//
// RCT_EXPORT_METHOD(showAlert:(NSString *)msg) {
//
//   // We'll show UIAlerView to know listener successful.
//   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//   dispatch_async(dispatch_get_main_queue(), ^{
//     [alert show];
//   });
//
//
// }
//
// - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//   {
//     _hasListeners = YES;
//     [self sendEventWithName:@"CancelEvent" body:@"Tap on Cancel button from Objc"];
//   }
//     // Sent event tap on Cancel
//     // [self sendEventWithName:@"CancelEvent" body:@"Tap on Cancel button from Objc"];
//
//     // Sent event tap on Ok
//     // [self sendEventWithName:@"OKEvent" body:@"Tap on OK button from Objc"];
// }
//
// @end

// - (NSArray<NSString *> *)supportedEvents
// {
//   return @[@"video-progress"];
// }
// [self sendEventWithName:@"video-progress" body:@"Hello"];
// Will be called when this module's first listener is added.
// - (void)startObserving
// {
//   hasListeners = YES;
//   // [self sendEventWithName:@"video-progress" body:@"Hello"];
// }
//
// // Will be called when this module's last listener is removed, or on dealloc.
// - (void)stopObserving
// {
//   hasListeners = NO;
// }
//
// - (void)calendarEventReminderReceived:(NSNotification *)notification
// {
//   NSString *eventName = notification.userInfo[@"video-progress"];
//   if (hasListeners) { // Only send events if anyone is listening
//     [self sendEventWithName:@"video-progress" body:@"Hello"];
//   }
// }

// RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
// {
//   RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
//   [self sendEventWithName:@"video-progress" body:@"Hello"];
// }
// @end
