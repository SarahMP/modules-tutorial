// CalendarManager.h
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTViewManager.h>

@interface CalendarManager : RCTEventEmitter <RCTBridgeModule,UIAlertViewDelegate>
@end

// #import <React/RCTBridgeModule.h>
// #import <UIKit/UIKit.h>
// // #import <RCTViewManager.h>
// // #import <RCTEventEmitter.h>
//
// @interface CalendarManager : RCTEventEmitter <RCTBridgeModule,UIAlertViewDelegate>
//
// @end
